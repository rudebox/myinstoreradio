<?php

/*
 * Template Name: Pricing template
 */

get_template_part('parts/header'); the_post(); ?>

<main>
	
	<?php get_template_part('parts/page', 'header'); ?>

	<section class="pricing">
		<div class="wrap hpad clearfix">
			<?php
				require_once( trailingslashit( get_stylesheet_directory() ) . 'parts/webservice.php' ); 

				$geoplugin = new geoPlugin();
				$geoplugin->locate(); 

				//fetch IP address
				$geoPlugin_array = unserialize(file_get_contents('http://www.geoplugin.net/php.gp?ip=' . WEBSERVICE::get_client_ip())); 
								
				$country_row = WEBSERVICE::GetCountries($geoPlugin_array['geoplugin_countryCode'] ? $geoPlugin_array['geoplugin_countryCode'] : 'DK')->value[0];

				$country_iso = $country_row['iso'];
				$country_name = $country_row['printable_name'] ? $country_row['printable_name'] : 'Other';
				$country_id = $country_row['id'];
				
				$form_id = ( $country_iso == 'DK' ? 4 : ( $country_iso == 'US' ? 30 : 5 ) );

				if ($country_iso == '')
					$prices = WEBSERVICE::GetPrices()->value;
				else
					$prices = WEBSERVICE::GetPrices(null, $country_iso)->value;	
					
				add_filter( 'gform_pre_render_' . $form_id, 'populate_pricing_form' );

				//Note: when changing choice values, we also need to use the gform_pre_validation so that the new values are available when validating the field.
				add_filter( 'gform_pre_validation_' . $form_id, 'populate_pricing_form' );
					
				//Note: when changing choice values, we also need to use the gform_admin_pre_render so that the right values are displayed when editing the entry.
				add_filter( 'gform_admin_pre_render_' . $form_id, 'populate_pricing_form' );
					
				//Note: this will allow for the labels to be used during the submission process in case values are enabled
				add_filter( 'gform_pre_submission_filter_' . $form_id, 'populate_pricing_form' );
								
				function populate_pricing_form( $form ) {
					global $country_name, $prices;
					
					foreach( $form['fields'] as &$field )  {
						if ( $field->id == 30 )
							$field->content = "Country<br>" . $country_name;
						
						if ( $field->id == 14 )
							$field->basePrice = sprintf('%.2f', floatval($prices[0]["Price"]) + floatval($prices[0]["CopyrightsPrLocation"]));

						if ( $field->id == 15 )
							$field->basePrice = sprintf('%.2f', floatval($prices[1]["Price"]) + floatval($prices[1]["CopyrightsPrLocation"]));
					
						if ( $field->id == 16 )
							$field->basePrice = sprintf('%.2f', floatval($prices[2]["Price"]) + floatval($prices[2]["CopyrightsPrLocation"]));

						if ( $field->id == 24 )
							$field->basePrice = sprintf('%.2f', floatval($prices[0]["Shipping1"]));
					}
					
					return $form;
				}					

				add_filter( 'gform_currency', 'change_currency' );
				function change_currency( $currency ) {
					global $prices;
					return $prices[0]['CurrencyCode'];
				}				

				gravity_form( $form_id, $display_title = false, $display_description = false, $display_inactive = false, $field_values = null, $ajax = false, $tabindex, $echo = true );

				if ($_POST['input_13']) {
					if ($_POST['input_13'] == '6 months') {
						$price_id = $prices[0]['id'];
						$_POST['nr_of_players'] = $_POST['input_17'];
					}

					if ($_POST['input_13'] == '12 months') {
						$price_id = $prices[1]['id'];
						$_POST['nr_of_players'] = $_POST['input_19'];
					}

					if ($_POST['input_13'] == '24 months') {
						$price_id = $prices[2]['id'];
						$_POST['nr_of_players'] = $_POST['input_20'];
					}

					$price = WEBSERVICE::GetPrices($price_id)->value[0];

					$shippingPrice = floatval($price['Shipping1']);

					$playersPrice = intval($_POST["nr_of_players"]) * floatval($price['Player']);
					$licensePrice = intval($price["Months"]) * floatval($_POST['nr_of_players']) * (floatval($price["Price"]) + floatval($price["CopyrightsPrLocation"]));
					$totalPrice = $shippingPrice + $playersPrice + $licensePrice;
					
					$sPaymentText = nl2br($_POST['nr_of_players'] . " player(s) $playersPrice 0.00 Shipping $price[CurrencyCode] $shippingPrice 1 license(s) of $price[Months] months $price[CurrencyCode] $licensePrice Total price $price[CurrencyCode] $totalPrice");
					
					$params = array(
						"sCompany" => $_POST['input_1'],
						"sVAT" => $price['VAT'],
						"sAddress" => $_POST['input_7'],
						"sCity" => $_POST['input_8'],
						"sPhone" => $_POST['input_4'],
						"sZipCode" => $_POST['input_9'],
						"sContact" => $_POST['input_10'],
						"sEMail" => $_POST['input_2'],
						"sComments" => $_POST['input_3'],
						"nTotalPrice" => $totalPrice,
						"sCurrencyCode" => $price['CurrencyCode'],
						"sPaymentText" => $sPaymentText
					);

					$res = WEBSERVICE::SignUp($params);
					
					if ($res->code == 'OK')
						$token = $res->value[0]['Token'];

					if ($token) {
						$payment = (object)WEBSERVICE::GetPayPal($token)->value[0];								
						?>

						<form name="frmPaypal" id="frmPaypal" action="https://www.paypal.com/cgi-bin/webscr" method="post">
							<input type="hidden" name="business" value="finance@myinstoreradio.com" />
							<input type="hidden" name="item_name" value="<?=$payment->Title?>"/>
							
							<?php
								if ($payment->IsRecurring) {?>
									<input type="hidden" name="cmd" value="_xclick-subscriptions" />
									<input type="hidden" name="a3" value="<?=$payment->Price?>" />					
									<input type="hidden" name="p3" value="1" />
									<input type="hidden" name="t3" value="M" />								 
									<input type="hidden" name="src" value="1" />
								<?php } else { ?>
									<input type="hidden" name="cmd" value="_xclick" />
									<!--<input type="hidden" name="lc" value="DK" />-->
									<input type="hidden" name="button_subtype" value="services" />
									<input type="hidden" name="amount" value="<?=$payment->Price?>" />	
									<input type="hidden" name="quantity" value="1" />	
								<?php } ?>
							
							<input type="hidden" name="currency_code" value="<?=$payment->CurrencyCode?>" />	
							<input type="hidden" name="custom" value="<?=$payment->Token?>" />
							<input type="hidden" name="notify_url" value="<?=get_site_url()?>/payment/ipn.php" />
						</form>
					
						<script type="text/javascript">
							$(function() {
								setTimeout(function() {
									$('#frmPaypal').submit();
								}, 1000);
							});
						</script>		
					<?php			
					
					}
				}
			?>
		</div>
	</section>

</main>

<?php get_template_part('parts/footer'); ?>