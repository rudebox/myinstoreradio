<?php

/*
 * Template Name: Easy install template
 */

get_template_part('parts/header'); the_post(); ?>

<main>
	
	<?php get_template_part('parts/page', 'header'); ?>

	<section class="install">
		<div class="wrap hpad clearfix center">
			<!-- get first frame -->
			<img class="install__sequence" id="sequence" src="<?php echo get_template_directory_uri(); ?>/assets/img/frames/MIR_Install_00000.png" alt="mir_player">
			</img>

			<video class="install__video" autoplay preload="auto" controls loop muted="muted" volume="0">
			<source src="<?php echo get_template_directory_uri(); ?>/assets/img/easy_install.mp4" type="video/mp4" codecs="avc1, mp4a">
			<source src="<?php echo get_template_directory_uri(); ?>/assets/img/easy_install.webm" type="video/webm" codecs="vp8">
			<source src="<?php echo get_template_directory_uri(); ?>/assets/img/easy_install.ogg" type="video/ogg" codecs="theora, vorbis">
			Your browser doesn't support HTML5 video. upgrade your browser to improve your experience.
			</video>

			<div class="install__scroll center flex flex--hvalign">Scroll <img class="install__scroll--icon" src="<?php echo get_template_directory_uri() . '/assets/img/angle-arrow-down.svg' ?>" alt="scroll"></div>
		</div>
	</section>

</main>

<?php get_template_part('parts/footer'); ?>