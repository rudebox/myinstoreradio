<?php
    error_reporting(0);
				
    class WEBSERVICE {
        public static $client;
        public static $db;
        
        public static function GetWebServiceClient()
        {
            $https_opts = array(
                'ssl' => array(
                    'verify_peer' => false,
                    'verify_peer_name' => false
                )
            );
            
            $url = "https://mirweb01.myinstoreradio.com/Services/MyInstoreRadioV2Test/svcMIR.asmx?wsdl";
            $client = new SoapClient($url, array( 
                'stream_context' => stream_context_create($https_opts),
                'verifypeer' => false,
                'verifyhost' => false,
                'trace' => false,  
                'cache_wsdl' => WSDL_CACHE_MEMORY,
                'exceptions' => true, 
                'connection_timeout'=> 99999, 
                'features' => SOAP_USE_XSI_ARRAY_TYPE, 
                'compression' => SOAP_COMPRESSION_ACCEPT | SOAP_COMPRESSION_GZIP,
                'soap_version' => SOAP_1_2,
                'encoding' => 'UTF-8'			   
            ));		
                            
            return $client;	
        }
        
        public static function Initialize()
        {
            if (!isset(self::$client)) {
                @ini_set("soap.wsdl_cache_enabled", "0"); 
                @ini_set("default_socket_timeout", '500');
                
                self::$client = self::GetWebServiceClient();
            }
        }

        public static function initialize_db()
        {            
            if (self::$db) return;

            self::$db = new PDO("sqlsrv:Server=MIRSQL02;Database=MyInstoreRadio", null, null, array(PDO::SQLSRV_ATTR_DIRECT_QUERY => true));
            			
            self::$db->setAttribute(PDO::ATTR_CASE, PDO::CASE_LOWER);
            self::$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            self::$db->setAttribute(PDO::ATTR_ORACLE_NULLS, PDO::NULL_TO_STRING);
            self::$db->setAttribute(PDO::SQLSRV_ATTR_ENCODING, PDO::SQLSRV_ENCODING_UTF8);
        }

        function get_client_ip() {
            $ipaddress = '';
            if (isset($_SERVER['HTTP_CLIENT_IP']))
                $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
            else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
                $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
            else if(isset($_SERVER['HTTP_X_FORWARDED']))
                $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
            else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
                $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
            else if(isset($_SERVER['HTTP_FORWARDED']))
                $ipaddress = $_SERVER['HTTP_FORWARDED'];
            else if(isset($_SERVER['REMOTE_ADDR']))
                $ipaddress = $_SERVER['REMOTE_ADDR'];
            else
                $ipaddress = 'UNKNOWN';
            return $ipaddress;
        }


        public static function Execute($sql, $array = array()) {
            self::initialize_db();

			$res = self::$db->prepare($sql);
			return $res->execute($array);
		}
	
		public static function GetOne($sql, $array = array(), $fetch_type=PDO::FETCH_OBJ) {
            self::initialize_db();

			$res = self::$db->prepare($sql);
			$res->execute($array);

			return $res->fetch($fetch_type);
		}

		public static function GetAll($sql, $array = array(), $fetch_type=PDO::FETCH_OBJ) {
            self::initialize_db();

			$res = self::$db->prepare($sql);
			$res->execute($array);

			return $res->fetchAll($fetch_type);
        }
        
        public static function FieldNameToCaption($fieldName)
        {
            return preg_replace('/([a-z])([A-Z])/', '$1 $2', $fieldName);			
        }
            
        public static function DecodeXMLDataset($xml)
        {
            $res = FUNCTIONS::xml2array($xml->any);
            $result = $res['diffgr:diffgram']['NewDataSet']['Table'];
            unset($result['0_attr']);
            
            $r = array();
                    
            foreach($result as $k => $v) {
                $val = array();
                
                if (!is_array($v)) 
                    $val = $v;
                else				
                    foreach($v as $kk => $vv)
                        $val[strtolower($kk)] = $vv;
                        
                $r[is_numeric($k) ? $k : strtolower($k)] = $val;
            }
                
            return $r;
        }
        
        public static function PlayerStreamNow($sEthernetMACAddress)
        {
            self::Initialize();
            
            $initParams = array();
            $initParams['sEthernetMACAddress'] = $sEthernetMACAddress;
            
            return json_decode(self::$client->jsonPlayerStreamNow($initParams)->jsonPlayerStreamNowResult);
        }
        
        
        public static function TestServerReachable($sEthernetMACAddress)
        {
            self::Initialize();
            
            $initParams = array();
            $initParams['sEthernetMACAddress'] = $sEthernetMACAddress;
            
            return json_decode(self::$client->jsonTestServerReachable($initParams)->jsonTestServerReachableResult);
        }
        
        public static function TestServerReachableSendMailToSupport($sEthernetMACAddress, $sContactPerson, $sEmail, $sPhone)
        {
            self::Initialize();
            
            $initParams = array();
            $initParams['sEthernetMACAddress'] = $sEthernetMACAddress;
            $initParams['sContactPerson'] = $sContactPerson;
            $initParams['sEmail'] = $sEmail;
            $initParams['sPhone'] = $sPhone;
            
            return json_decode(self::$client->jsonTestServerReachableSendMailToSupport($initParams)->jsonTestServerReachableSendMailToSupportResult);
        }

        public static function GetPrices($sID=null, $sISO=null)
        {
            self::Initialize();
            
            $initParams = array(
                'sID' => $sID,
                'sISO' => $sISO
            );

            $res = json_decode(self::$client->jsonGetPrices($initParams)->jsonGetPricesResult);
            $res->value = json_decode($res->value, true);

            return $res;
        }

        public static function GetCountries($sISO=null)
        {
            self::Initialize();
            
            $initParams = array(
                'sID' => $sID,
                'sISO' => $sISO
            );
            
            $res = json_decode(self::$client->jsonGetCountries($initParams)->jsonGetCountriesResult);
            $res->value = json_decode($res->value, true);

            return $res;
        }

        public static function SignUp($data = array())
        {
            self::Initialize();
                        
            $res = json_decode(self::$client->jsonSignup($data)->jsonSignupResult);
            $res->value = json_decode($res->value, true);

            return $res;
        }        

        public static function GetPayPal($sToken)
        {
            self::Initialize();
            
            $initParams = array(
                'sToken' => $sToken
            );
            
            $res = json_decode(self::$client->jsonGetPayPal($initParams)->jsonGetPayPalResult);
            $res->value = json_decode($res->value, true);

            return $res;
        }
    }
?>