<?php 
/**
* Description: Lionlab employee repeater field group layout
*
* @package Lionlab
* @subpackage Lionlab
* @since Version 1.0
* @author Kaspar Rudbech
*/

//sections settings
$bg = get_sub_field('bg');
$margin = get_sub_field('margin');
$title = get_sub_field('header');
$border = get_sub_field('border');


$department = get_sub_field('employees_department');

if (have_rows('employees') ) :
?>

<section class="employee padding--<?php echo esc_attr($margin); ?> <?php echo esc_attr($bg); ?>--bg border--<?php echo esc_attr($border); ?>">
	<div class="wrap hpad">
		<?php if ($title) : ?>
		<h2 class="employee__header"><?php echo esc_html($title); ?></h2> 
		<?php endif; ?>

		<?php if ($department) : ?>
		<h3 class="employee__department"><?php echo esc_html($department); ?></h3> 
		<?php endif; ?>
		<div class="row flex flex--wrap">
			<?php 
				while (have_rows('employees') ) : the_row();
				$img = get_sub_field('img');
				$text = get_sub_field('text');

 			 ?>

 			 <div class="col-sm-6 employee__item">
 			 	<div class="col-sm-5 no-gutter">
 			 		<?php if ($img) : ?>
 			 		<img class="employee__img" src="<?php echo esc_url($img['url']); ?>" alt="<?php echo esc_attr($img['alt']); ?>">
 			 		<?php endif; ?>
 			 	</div>

 			 	<div class="col-sm-7 employee__text">
 			 		<?php echo $text; ?>
 			 	</div>
 			 </div>

 			<?php endwhile; ?>
		</div>
	</div>
</section>
<?php endif; ?>