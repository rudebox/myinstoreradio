<?php 
/**
* Description: Lionlab ad-examples repeater field group layout
*
* @package Lionlab
* @subpackage Lionlab
* @since Version 1.0
* @author Kaspar Rudbech
*/

//sections settings
$bg = get_sub_field('bg');
$margin = get_sub_field('margin');
$title = get_sub_field('header');

if (have_rows('examples') ) :
?>

<section class="ad-examples <?php echo $bg; ?>--bg padding--<?php echo $margin; ?>">
	<div class="wrap hpad">
		<h2 class="link-boxes__header"><?php echo esc_html($title); ?></h2>
		<div class="row flex flex--wrap ad-examples__row">
			<?php while (have_rows('examples') ) : the_row(); 
				$title = get_sub_field('title');
				$text = get_sub_field('text');
				$audio = get_sub_field('file');
			?>

			<div class="ad-examples__wrap">
				<div class="col-sm-6 ad-examples__item">
					<h3 class="ad-examples__title"><?php echo esc_html($title); ?></h3>
					<p><?php echo $text; ?></p>
				</div>

				<div class="col-sm-6 ad-examples__audio">
					<?php echo do_shortcode('[audio src="' . esc_url($audio) . '"]'); ?>
				</div>
			</div>
			<?php endwhile; ?>
		</div>
	</div>
</section>
<?php endif; ?>