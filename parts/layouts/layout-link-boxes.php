<?php 
/**
* Description: Lionlab link-boxes repeater field group layout
*
* @package Lionlab
* @subpackage Lionlab
* @since Version 1.0
* @author Kaspar Rudbech
*/

//sections settings
$bg = get_sub_field('bg');
$margin = get_sub_field('margin');
$title = get_sub_field('header'); 

if (have_rows('linkbox') ) :
?>

<section class="link-boxes <?php echo esc_attr($bg); ?>--bg padding--<?php echo esc_attr($margin); ?>">
	<div class="wrap hpad">
		<h2 class="link-boxes__header"><?php echo esc_html($title); ?></h2>
		<div class="row flex flex--wrap">
			<?php while (have_rows('linkbox') ) : the_row(); 
				$title = get_sub_field('title');
				$text = get_sub_field('text');
				$icon = get_sub_field('icon');
				$class = get_sub_field('class');
			?>

			<div class="col-sm-4 link-boxes__item <?php echo esc_attr($class); ?>">
				<div class="link-boxes__wrap">
					<img src="<?php echo esc_url($icon['url']); ?>" alt="<?php echo esc_attr($icon['alt']); ?>">					
					<h3 class="link-boxes__title"><?php echo esc_html($title); ?></h3>	
				</div>
			
				<?php echo $text; ?>

			</div>
			<?php endwhile; ?>
		</div>
	</div>
</section>
<?php endif; ?>