<?php 
/**
* Description: Mac adress lookup field group layout
*/

//sections settings
$bg = get_sub_field('bg');
$margin = get_sub_field('margin');
$title = get_sub_field('header');
$border = get_sub_field('border');
?>

<section class="mac-lookup padding--<?php echo esc_attr($margin); ?> <?php echo esc_attr($bg); ?>--bg border--<?php echo esc_attr($border); ?>">
	<div class="wrap hpad">
		<?php if ($title) : ?>
			<h2 class="mac-lookup__header"><?php echo esc_html($title); ?></h2>
		<?php endif; ?>
		<div class="row">
			<div class="col-sm-6">
				<img src="<?php echo get_template_directory_uri(); ?>/assets/img/MIR_player_mac_adress.gif" alt="MIR_player_mac_adress">

				<?php
					require_once( trailingslashit( get_stylesheet_directory() ) . 'parts/webservice.php' );				
				?>

			<div>
				<h3>Enter the MIR Player MAC address located on the bottom of the device</h3>
				
				<form method="GET" id="js-mac-adress-lookup" action="<?php echo $_SERVER['REQUEST_URI']; ?>#server-results">
					<ul class="mac-lookup__form">
						<li class="mac-lookup__form--list">
							<label class="mac-lookup__form--label">Device MAC Address</label>
							<input class="mac-lookup__form--input" placeholder="00:66:de:0a:33:22" type="text" name="mac" value="<?php echo $_REQUEST['mac']; ?>" />
						</li>
						<input class="btn btn--orange btn--large" type="submit" value="Verify" />
					</ul>
				</form>
				
			</div>

			</div>

			<div class="col-sm-6 mac-lookup__status">
				<div id="server-results" class="mac-lookup__status--server">
					<?php		
						if ($_REQUEST['mac'] != '') {
							try {		
								$result = WEBSERVICE::PlayerStreamNow($_REQUEST['mac']);
								
							
								if ($result->code === "OK") {
									$value = json_decode($result->value);
									echo "This device is <b><span style='color:" . ($value[0]->Connected ? "green'>ONLINE" : "red'>OFFLINE") . "</span></b>";
								} else throw new Exception($result->message);
							 }
							 catch(Exception $exc) {
								 echo $exc->getMessage();
							 }
						}
						
						/* Server checks below */
						
						if ($_REQUEST['mac'] != '') {
							try {		
								$result = WEBSERVICE::TestServerReachable($_REQUEST['mac']);
								
								if ($result->code === "OK") {
									
									$value = json_decode($result->value);
									
									echo "<br>";
									
									echo "<br><span>". ($value[0]->PlayerServer ? "&#10003" : "&#10008") ."</span> Player Service";
									echo "<br><span>". ($value[0]->StreamingServer ? "&#10003" : "&#10008") ."</span> Streaming Service";
									echo "<br><span>". ($value[0]->LoggingServer ? "&#10003" : "&#10008") ."</span> Logging Service";
									
									
								} else throw new Exception($result->message);
							 }
							 catch(Exception $exc) {
								 echo $exc->getMessage();
							 }
						}

						/* Send mail to support below */
						
						
						if ($_REQUEST['contactperson'] != '') {
							
							try {	
						
								$result = WEBSERVICE::TestServerReachableSendMailToSupport($_REQUEST['mac'],$_REQUEST['contactperson'],$_REQUEST['email'],$_REQUEST['phone']);
							
								if ($result->code === "OK") {
									
									$value = json_decode($result->value);
									
									echo "<br>";
									
									echo "<br><span>". ($value[0]->EmailSent ? "&#10003" : "&#10008") ."</span> Mail sent to support";
									
									
								} else throw new Exception($result->message);
							 }
							 catch(Exception $exc) {
								 echo $exc->getMessage();
							 }
						}
						
					?>	
				</div>

				<h3 class="mac-lookup__title center">Contact support</h3>
				<form method="GET" id="js-mac-adress-support" action="<?php echo $_SERVER['REQUEST_URI']; ?>#server-results">
					<ul class="mac-lookup__form">
						<li class="mac-lookup__form--list hidden">
							<input class="mac-lookup__form--input" type="text" name="mac" value="<?php echo $_REQUEST['mac']; ?>" />
						</li>
						<li class="mac-lookup__form--list">
							<label class="mac-lookup__form--label">Contact person *</label>
							<input class="mac-lookup__form--input" type="text" name="contactperson" value="<?php echo $_REQUEST['contactperson']; ?>" />
						</li>
						<li class="mac-lookup__form--list">
							<label class="mac-lookup__form--label">E-mail *</label>
							<input class="mac-lookup__form--input" type="text" name="email" value="<?php echo $_REQUEST['email']; ?>" />
						</li>
						<li class="mac-lookup__form--list">
							<label class="mac-lookup__form--label">Phone *</label>
							<input class="mac-lookup__form--input" type="text" name="phone" value="<?php echo $_REQUEST['phone']; ?>" />
						</li>
						<label>* Mandatory fields</label>
						<input class="btn btn--orange btn--large" type="submit" value="Send" />
					</ul>
				</form>
			</div>
		</div>
	</div>
</section>