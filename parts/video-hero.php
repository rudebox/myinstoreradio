<?php 
// Hero section

//fallback poster
$poster = get_field('video_poster');

$rows = get_field('video_hero');

if (have_rows('video_hero') ) :

//get random row
shuffle($rows); 

//counter
$i=0; 

?>

<section class="video">
			
	<?php 
	/**
	 * Video
	 **/ 

		//loop through rows
		foreach ($rows as $row) :

		$video_mp4 = $row['video_mp4'];
		$video_ogv = $row['video_ogv'];
		$video_webm = $row['video_webm'];

		$i++;

		//limit to one row
		if ($i <= 1) {
	?>
	
	<video id="video-hero" class="video__hero" autoplay preload="auto" loop muted="muted" volume="0" poster="<?php echo esc_url($poster['url']); ?>">
		<source src="<?php echo esc_url($video_mp4); ?>" type="video/mp4" codecs="avc1, mp4a">
		<source src="<?php echo esc_url($video_ogv); ?>" type="video/ogg" codecs="theora, vorbis">
		<source src="<?php echo esc_url($video_webm); ?>" type="video/webm" codecs="vp8, vorbis">
		Your browser doesn't support HTML5 video. upgrade your browser to improve your experience.
	</video>

	<?php } endforeach; ?>

	
</section>
<?php endif; ?>