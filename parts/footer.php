<div class="footer__tagline">
	#YourCustomersAreListening
</div>

<footer class="footer brown--bg" id="footer">
	<div class="wrap hpad">
		<div class="row flex clearfix footer__row">
			<?php 
				if (have_rows('footer_columns', 'options') ) : while (have_rows('footer_columns', 'options') ) :
					the_row();
				$title = get_sub_field('column_title');
				$text = get_sub_field('column_text');

				//count
				$index = get_row_index();
			 ?>

			 <div class="col-sm-2 footer__item footer__item--<?php echo esc_attr($index); ?>">
			 	<?php if ($title) : ?>
			 	<h4 class="footer__title"><?php echo esc_html($title); ?></h4>
			 	<?php endif; ?>
				<?php echo $text; ?>
			 </div>

			<?php endwhile; endif; ?>
		</div>
	</div>
	
	<div class="footer__copyrights">
		<div class="wrap hpad clearfix center">
			<p class="footer__copyrights--trademark">My Instore Radio, the My Instore Radio logo and the MIR player are trademarks and/or copyrights of My Instore Apps ApS.</p>

			<p class="footer__copyrights--rights">© <time datetime="<?php the_time('Y'); ?>"><?php the_time('Y'); ?></time> My Instore Apps ApS. All rights reserved.</p>
		</div>
	</div>
</footer>

<?php wp_footer(); ?>

</body>
</html>
