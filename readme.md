<h2>Installation</h2>
<p>The new theme requires a local installation of Node.js and Gulp</p>

<ol>
	<li><a target="_blank" href="https://nodejs.org/download/">Download</a> and install Node.js. 
After installing Node.js, retrieve a local version of Gulp by typing sudo npm install gulp-cli -g in Terminal</li>
</ol>

<h2>Starting a new project</h2>
<ol>
	<li>Open the folder in your editor of choice and start a new Terminal in the gulp folder</li>
	<li>Type sudo npm install to install the required node modules</li>
	<li>Modify your gulp script by changing the browsersync proxy to match your host</li>
	<li>Run gulp in your Terminal by typing gulp and hit enter </li>
	<li>... You are now up and running! Do your magic.</li>
</ol>
