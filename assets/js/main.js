jQuery(document).ready(function($) {

  //smooth scroll
  $('a[href^="#"]').on('click',function (e) {
    e.preventDefault();

    var target = this.hash;
    var $target = $(target);

    $('html, body').stop().animate({
      'scrollTop': $target.offset().top
    }, 900, 'swing', function () {
      window.location.hash = target;
    });
  });

  //menu toggle
  $('.nav-toggle').click(function(e) {
    e.preventDefault();
    $('.nav--mobile').toggleClass('is-open');
    $('body').toggleClass('is-fixed');
  });


  //device lookup on submit animation  
  if (window.location.href.indexOf("?mac") > -1) {
    $('.mac-lookup__status').css({
      'opacity': '1', 
      'margin-top': '0rem' 
    }); 
  }

  //wrap GF in columns
  $('.gf_left').wrapAll('<div class="gf_column"></div>');
  $('.gf_right').wrapAll('<div class="gf_column"></div>');

  //wrap price box in flex container
  $('.gf_price_month').wrapInner('<div class="flex flex--hvalign-reverse"></div>');


  //increment by 1 and add to class
  $i = 0; 

  $('.gfield_radio li > input').each(function() {
    $i++;
    $(this).addClass('gfield_months-' + $i);
    $(this).addClass('gfield_months');
    $(this).css('width', "100% !important");
  });  


  //set first to active
  $('.gfield_months-1').addClass('is-active');
  

  //add class on click
  $('.gfield_radio li > input').click(function() {
    // remove classes from all
    $('.gfield_radio li > input').removeClass('is-active');
    // add class to the one we clicked
    $(this).addClass('is-active');
  });

  //override default style for GF radio buttons
  $('.gfield_radio li > input').attr('style', 'width: 100% !important');
      

}); 