// Avoid `console` errors in browsers that lack a console.
(function() {
    var method;
    var noop = function () {};
    var methods = [
        'assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error',
        'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log',
        'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd',
        'timeline', 'timelineEnd', 'timeStamp', 'trace', 'warn'
    ];
    var length = methods.length;
    var console = (window.console = window.console || {});

    while (length--) {
        method = methods[length];

        // Only stub undefined methods.
        if (!console[method]) {
            console[method] = noop;
        }
    }
}());

// Place any jQuery/helper plugins in here.
;jQuery(document).ready(function($) {
	$('ul.nav__menu').find('> li').click(
	    function() {
	        $(this).find('> ul').toggleClass('is-visible');
	    }
	);

	$('ul.nav__dropdown').find('> li').click(
	    function(e) {
	        e.stopPropagation()
	        $(this).find('> ul').toggleClass('is-visible');
	    }
	);
});

;jQuery(document).ready(function($) {

  /*
  Javascript-video-scrubber
  Created by Gary Hepting and the Dev Team at Emerge Interactive
  Fork, follow and watch on Github at https://github.com/ghepting/javascript-video-scrubber
  Visit Emerge Interctive at http://emergeinteractive.com/
  */

  //fire script if template is easy install
  if($('body').hasClass('easy-install') && $(window).width() > 768)  {

    //get template url
    var templateUrl = $('base').attr('href');

    var step = 0; // visible frame
    var targetStep = 0; // frame to animate to
    var images = new Array; // stores all of the frames for quick access
    var scrollPos; // scroll position of the window
    var totalFrames = 662; // the number of images in the sequence of PNG files (this could be calculated server-side by scanning the frames folder)

    window.requestAnimFrame = (function(){ // reduce CPU consumption, improve performance and make this possible
      return  window.requestAnimationFrame       || 
          window.webkitRequestAnimationFrame || 
          window.mozRequestAnimationFrame    || 
          window.oRequestAnimationFrame      || 
          window.msRequestAnimationFrame     || 
          function( callback ){
          window.setTimeout(callback, 1000 / 60);
          };
    })();

    (function animloop(){ // the smoothest animation loop possible
      requestAnimFrame(animloop);
      targetStep = Math.max( Math.round( getYOffset() / 30 ) , 1 ); // what frame to animate to
      if(targetStep != step ) { step += (targetStep - step) / 5; } // increment the step until we arrive at the target step
      changeFrame();
    })();

    function changeFrame() {
      var thisStep = Math.round(step); // calculate the frame number
      if(images.length > 0 && images[thisStep]) { // if the image exists in the array
        if(images[thisStep].complete) { // if the image is downloaded and ready
          if($('#sequence').attr('src') != images[thisStep].src) { // save overhead
            $('#sequence').attr('src',images[thisStep].src); // change the source of our placeholder image
          }
        }
      }
    }

    function resizeAdjustments() { // fit everything to the screen
      $('main').css('height',(totalFrames*32)+'px'); // increase the height of the document 30 pixels for every frame in the PNG sequence

      var image_width   = $('#sequence').css('width').replace('px','');
      var image_height  = $('#sequence').css('height').replace('px','');
      var height_ratio  = image_height / document.body.clientHeight;
      var width_ratio   = image_width / document.body.clientWidth;

      if (height_ratio < width_ratio) {
        $('#sequence').css('top',0); // reposition the video image
        var difference = parseInt(image_width-document.body.clientWidth); // calculate the difference we need to accomodate for

        if (document.body.clientWidth < image_width) {
          // reposition the video image from the left
          $('#sequence').css('left',(difference / 2)*-1); 
        }

        else {
          $('#sequence').css('left',0);
        }
      }

      else {
        $('#sequence').css('left',0);

        // calculate the difference we need to accomodate for
        var difference = parseInt(image_height-document.body.clientHeight); 

        if (document.body.clientHeight < image_height) {
          // reposition the video image from the top
          $('#sequence').css('top',(difference/2)*-1); 
        }

        else {
          $('#sequence').css('top',0);
        }

        //resize image to fill the width of the viewport
        $('#sequence').css('width','100%'); 
        $('#sequence').css('height','auto');
      }
    }

    // get distance scrolled from the top
    function getYOffset() { 
      var pageY;

      if (typeof(window.pageYOffset)=='number') {
        pageY=window.pageYOffset;
      }

      else {
        //IE
        pageY=document.documentElement.scrollTop; 
      }

      return pageY;
    }

    // pad numbers with leading zeros for JPEG sequence file names
    function pad(number, length) { 
      var str = '' + number; while (str.length < length) { str = '0' + str; } return str;
    }


    // image filenames in frames folder should be "MIR_Install_00000.png" through "MIR_Install_00000.png"
    for(i = 0; i < totalFrames; i++) { // loop for each image in sequence
      images[i] =  new Image(); // add image object to array
      images[i].src = templateUrl + "/assets/img/frames/MIR_Install_"+pad(i, 5)+".png"; // set the source of the image object
    }

    // the number of images in the sequence of PNG files (this could be calculated server-side by scanning the frames folder)
    totalFrames = 662; 
    // adjust the size of video placeholder image to fit the screen and keep aspect ratio (zoom crop)
    resizeAdjustments(); 

    //sticky header and title
    (function() {
      var $header = $('.header');
      var $hero = $('.page__hero');

      $(window).on('load resize scroll', function(e) {
        var scrollTop = $(window).scrollTop();

        if ( scrollTop ) {
          $header.addClass('is-sticky');
          $hero.addClass('is-sticky');
        } else {
          $hero.removeClass('is-sticky');
          $header.removeClass('is-sticky');
        }
      });

    })();


    //check if user has scrolled to end or top of document   
    $(window).scroll(function() {
      footerOffset = $('footer').offset().top;
      windowHeight = $(window).scrollTop() + $(window).height();

      windowTop = $(window).scrollTop();
      headerOffset = $('header').offset().top;


      if (windowHeight >= footerOffset) {
        $('.install__scroll--icon').addClass('is-bottom');
        $('.install__scroll--icon').removeClass('is-start');
        $('.header').removeClass('is-sticky');
        $('.page__hero').removeClass('is-sticky'); 
      }

      else if (windowTop <= headerOffset) {
        $('.install__scroll--icon').addClass('is-start');
        $('.install__scroll--icon').removeClass('is-bottom');
      }

    });
    
  }
  
}); ;jQuery(document).ready(function($) {

  //smooth scroll
  $('a[href^="#"]').on('click',function (e) {
    e.preventDefault();

    var target = this.hash;
    var $target = $(target);

    $('html, body').stop().animate({
      'scrollTop': $target.offset().top
    }, 900, 'swing', function () {
      window.location.hash = target;
    });
  });

  //menu toggle
  $('.nav-toggle').click(function(e) {
    e.preventDefault();
    $('.nav--mobile').toggleClass('is-open');
    $('body').toggleClass('is-fixed');
  });


  //device lookup on submit animation  
  if (window.location.href.indexOf("?mac") > -1) {
    $('.mac-lookup__status').css({
      'opacity': '1', 
      'margin-top': '0rem' 
    }); 
  }

  //wrap GF in columns
  $('.gf_left').wrapAll('<div class="gf_column"></div>');
  $('.gf_right').wrapAll('<div class="gf_column"></div>');

  //wrap price box in flex container
  $('.gf_price_month').wrapInner('<div class="flex flex--hvalign-reverse"></div>');


  //increment by 1 and add to class
  $i = 0; 

  $('.gfield_radio li > input').each(function() {
    $i++;
    $(this).addClass('gfield_months-' + $i);
    $(this).addClass('gfield_months');
    $(this).css('width', "100% !important");
  });  


  //set first to active
  $('.gfield_months-1').addClass('is-active');
  

  //add class on click
  $('.gfield_radio li > input').click(function() {
    // remove classes from all
    $('.gfield_radio li > input').removeClass('is-active');
    // add class to the one we clicked
    $(this).addClass('is-active');
  });

  //override default style for GF radio buttons
  $('.gfield_radio li > input').attr('style', 'width: 100% !important');
      

}); 